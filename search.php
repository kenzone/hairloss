<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css" title="currentStyle">
@import "css/demo_page.css";
@import "css/demo_table.css";
</style>

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
/* Time between each scrolling frame */
$.fn.dataTableExt.oPagination.iTweenTime = 100;

$.fn.dataTableExt.oPagination.scrolling = {
	"fnInit": function ( oSettings, nPaging, fnCallbackDraw )
	{
		var oLang = oSettings.oLanguage.oPaginate;
		var oClasses = oSettings.oClasses;
		var fnClickHandler = function ( e ) {
			if ( oSettings.oApi._fnPageChange( oSettings, e.data.action ) )
			{
				fnCallbackDraw( oSettings );
			}
		};

		var sAppend = (!oSettings.bJUI) ?
			'<a class="'+oSettings.oClasses.sPagePrevDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button">'+oLang.sPrevious+'</a>'+
			'<a class="'+oSettings.oClasses.sPageNextDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button">'+oLang.sNext+'</a>'
			:
			'<a class="'+oSettings.oClasses.sPagePrevDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button"><span class="'+oSettings.oClasses.sPageJUIPrev+'"></span></a>'+
			'<a class="'+oSettings.oClasses.sPageNextDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button"><span class="'+oSettings.oClasses.sPageJUINext+'"></span></a>';
		$(nPaging).append( sAppend );
		
		var els = $('a', nPaging);
		var nPrevious = els[0],
			nNext = els[1];
		
		oSettings.oApi._fnBindAction( nPrevious, {action: "previous"}, function() {
			/* Disallow paging event during a current paging event */
			if ( typeof oSettings.iPagingLoopStart != 'undefined' && oSettings.iPagingLoopStart != -1 )
			{
				return;
			}
			
			oSettings.iPagingLoopStart = oSettings._iDisplayStart;
			oSettings.iPagingEnd = oSettings._iDisplayStart - oSettings._iDisplayLength;
			
			/* Correct for underrun */
			if ( oSettings.iPagingEnd < 0 )
			{
			  oSettings.iPagingEnd = 0;
			}
			
			var iTween = $.fn.dataTableExt.oPagination.iTweenTime;
			var innerLoop = function () {
				if ( oSettings.iPagingLoopStart > oSettings.iPagingEnd ) {
					oSettings.iPagingLoopStart--;
					oSettings._iDisplayStart = oSettings.iPagingLoopStart;
					fnCallbackDraw( oSettings );
					setTimeout( function() { innerLoop(); }, iTween );
				} else {
					oSettings.iPagingLoopStart = -1;
				}
			};
			innerLoop();
		} );

		oSettings.oApi._fnBindAction( nNext, {action: "next"}, function() {
			/* Disallow paging event during a current paging event */
			if ( typeof oSettings.iPagingLoopStart != 'undefined' && oSettings.iPagingLoopStart != -1 )
			{
				return;
			}
			
			oSettings.iPagingLoopStart = oSettings._iDisplayStart;
			
			/* Make sure we are not over running the display array */
			if ( oSettings._iDisplayStart + oSettings._iDisplayLength < oSettings.fnRecordsDisplay() )
			{
				oSettings.iPagingEnd = oSettings._iDisplayStart + oSettings._iDisplayLength;
			}
			
			var iTween = $.fn.dataTableExt.oPagination.iTweenTime;
			var innerLoop = function () {
				if ( oSettings.iPagingLoopStart < oSettings.iPagingEnd ) {
					oSettings.iPagingLoopStart++;
					oSettings._iDisplayStart = oSettings.iPagingLoopStart;
					fnCallbackDraw( oSettings );
					setTimeout( function() { innerLoop(); }, iTween );
				} else {
					oSettings.iPagingLoopStart = -1;
				}
			};
			innerLoop();
		} );
	},
	
	"fnUpdate": function ( oSettings, fnCallbackDraw )
	{
		if ( !oSettings.aanFeatures.p )
		{
			return;
		}
		
		/* Loop over each instance of the pager */
		var an = oSettings.aanFeatures.p;
		for ( var i=0, iLen=an.length ; i<iLen ; i++ )
		{
			if ( an[i].childNodes.length !== 0 )
			{
				an[i].childNodes[0].className = 
					( oSettings._iDisplayStart === 0 ) ? 
					oSettings.oClasses.sPagePrevDisabled : oSettings.oClasses.sPagePrevEnabled;
				
				an[i].childNodes[1].className = 
					( oSettings.fnDisplayEnd() == oSettings.fnRecordsDisplay() ) ? 
					oSettings.oClasses.sPageNextDisabled : oSettings.oClasses.sPageNextEnabled;
			}
		}
	}
}
			
			$(document).ready(function() {
				$('#example').dataTable( {
					"sPaginationType": "scrolling"
				} );
			} );
</script>
       
<!--On load check 1st radio button, disable those textbox and the drop down list, and also reset function-->        
<script type="text/javascript">
function setvalue(){
document.getElementById('search3').checked = true
if (document.getElementById('search3').checked = true)
{ 
document.getElementById('ddl_state').disabled='';
document.getElementById('ddl_town').disabled='';
document.getElementById('txt_postcode').disabled='disabled';
}
}
window.onload = setvalue;


function disablefield(){ 
if (document.getElementById('search2').checked == 1)
{ 
document.getElementById('txt_postcode').disabled=''; 
}
else{ 
document.getElementById('txt_postcode').disabled='disabled';
}
if(document.getElementById('search3').checked == 1)
{
document.getElementById('ddl_state').disabled='';
document.getElementById('ddl_town').disabled=''; 
}
else{ 
document.getElementById('ddl_state').disabled='disabled';
document.getElementById('ddl_town').disabled='disabled'; 
}
}

function formreset()
{
if(document.getElementById('ddl_state').selectedIndex = 'selected')
{
document.getElementById('ddl_state').selectedIndex=null;
}
if(document.getElementById('ddl_town').selectedIndex = 'selected')
{
document.getElementById('ddl_town').selectedIndex=null;
}
if(document.getElementById('txt_postcode').value != '')
{
document.getElementById('txt_postcode').value=null;
}
}

function mypopup(id)
{
alert(id);
   //mywindow = window.open("deletedoctor.php?docid=", "mywindow", "location=1,status=1,scrollbars=1,width=100,height=100");
   //mywindow.moveTo(50, 50);
}

</script>

</head>
<body id="dt_example">

<form method="get" action="search.php" name="form1" >
<table border="0" width="100%" height="100%" style="padding-left:225px;">

<tr>
<td width="200px;" valign="top" style="padding-top:5px;"><input type="radio" name="search1" id="search3" onChange="disablefield();"/>Search by state and town</td>
<td>
<table>
<tr>
<td>State</td>
<td>
<?php

include "config/config.php";
	
$sql = "SELECT strStateName FROM doctor group by strStateName";

$result = mysql_query($sql);
		
		echo "<select name='ddl_state' id='ddl_state' OnChange='document.form1.submit();'>";
        echo "<option value=''>- Select State -</option>";
		
while($row = mysql_fetch_array($result))
{
$ddl_state = $row['strStateName'];
$newVariablestate = str_replace(" ", "_", $ddl_state);
$selected = ($_GET['ddl_state'] == $newVariablestate)?'selected="selected"':NULL;
		
		echo "<option value=".$newVariablestate." ".$selected.">".$newVariablestate."</option>";   
}
		echo "</select>";
	
include "config/con_close.php";

?>
</td>
</tr>

<tr><td>Town</td>
<td>
<?php

		include "config/config.php";
		
		$oldvariable = $_GET["ddl_state"];
        $newVariablestate = str_replace("_", " ", $oldvariable);
	
	    //replaces "_" to " "
	    $sql = "SELECT strTownName FROM doctor where strStateName='". $newVariablestate ."' group by strTownName";

		$result = mysql_query($sql);
		
		echo "<select name='ddl_town' id='ddl_town' >";
        echo "<option value=''>- Select Town -</option>";
		while($row = mysql_fetch_array($result))
   		{
		
		//replaces "" to "_"
		$ddl_town = $row['strTownName'];
        $newVariabletown = str_replace(" ", "_", $ddl_town);
		$selected = ($_GET['ddl_town'] == $newVariabletown)?'selected="selected"':NULL;
		
		echo "<option value=".$newVariabletown." ".$selected.">".$newVariabletown."</option>";
       
		}
		echo "</select>";
	
	include "config/con_close.php";
	

 ?>
</td>
</tr>
</table>
</td>
</tr>

<tr>
<td><input type="radio" name="search1" id="search2" onChange="disablefield();"/>Search by Postcode</td>
<td>Post Code<input type="text" name="postcode" id="txt_postcode"/></td>
</tr>

<tr>
<td></td>
<td><input type="button" value="Reset" name="reset" onclick="formreset()"><input type="submit" name="btn_submit" id="btn_submit" value="submit"/></td>
</tr>

</table>

</form>

<!--Start the Paging for the grid search-->
<div id="container">
            
<div id="demo">
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
            <th align="center">No</th>
			<th align="center">Clinic Name</th>
			<th align="center">Address1</th>
			<th align="center">Address2</th>
			<th align="center">Town</th>
			<th align="center">State</th>
            <th align="center">PostCode</th>
            <th align="center">Phone</th>
		</tr>
	</thead>
	<tbody>
    <?php
if(isset($_GET['btn_submit']))
{
include "functions.php";

if($_GET['ddl_state'] != "" && $_GET['ddl_town'] != ""){
$state = $_GET['ddl_state'];
$newVariablestate = str_replace("_", " ", $state);
$town = $_GET['ddl_town'];
$newVariabletown = str_replace("_", " ", $town);

search_doctor_by_state_and_town($newVariablestate,$newVariabletown);
}else if ($_GET['postcode'] != "")
{
$postcode = $_GET['postcode'];
search_doctor_postcode($postcode);
}

?>
    <?php
	
	}
	else
	{
	}

?>
	</tbody>
	<tfoot>
		<tr>
            <th align="center">No</th>
			<th align="center">Clinic Name</th>
			<th align="center">Address1</th>
			<th align="center">Address2</th>
			<th align="center">Town</th>
			<th align="center">State</th>
            <th align="center">PostCode</th>
            <th align="center">Phone</th>
		</tr>
	</tfoot>
</table>
			</div>
		</div>
	</body>
</html>
